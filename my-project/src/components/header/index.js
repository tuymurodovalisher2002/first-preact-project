import { h } from "preact";
import { Link } from "preact-router/match";
import style from "./style.css";
import { Button } from "antd";

const Header = () => (
  <header class={style.header}>
    <h1>Preact App Alisher</h1>
    <nav>
      <Link activeClassName={style.active} href="/">
        Home
      </Link>
      <Link activeClassName={style.active} href="/profile">
        Me
      </Link>
      <Link activeClassName={style.active} href="/profile/alisher">
        John
      </Link>
      <Button>Alisher</Button>
    </nav>
  </header>
);

export default Header;
